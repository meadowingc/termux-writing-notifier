# Termux Notifier for Writing Progress

Scripts to run on Termux (in Android) to get daily notifications about writing goals.

The primary aim of this project is to create a simple, yet effective notification system that tracks my writing progress across different text files. It's a custom utility designed to run on Termux, specifically aiding in keeping tabs on the number of written words and pages.

Having these notifications pop up on my phone encourages consistency and motivation by making progress tangible.

Although this is a personal set of scripts I created for myself I'm still sharing the code publicly in case anyone finds any use for it

Contributions, suggestions, or any form of feedback are also welcome, as they can help further refine the project.

