# This method sends a notification to an Android device using the Termux API.
# content: String specifying the main content of the notification
# title: String specifying the title of the notification, defaults to "Notifier"
# group: String specifying the notification group, defaults to a specific identifier "mdw-termux-notifier-2414" for grouping related notifications
# id: Optional Integer that specifies a unique identifier for the notification, allowing it to be updated later if needed
def show_notification(content, title = "Notifier", group = "mdw-termux-notifier-2414", id = nil)
  # Checks if an 'id' is provided; if not, the 'id' String stays empty
  # If an ID exists, it constructs the necessary CLI argument for notification identification
  if id.nil?
    id = ""
  else
    id = " --id #{id}"
  end

  # Executes the Termux notification command with the given parameters
  `termux-notification #{id} --title "#{title}" --content "#{content}" --group "#{group}" `
end

# Counts the number of words in all Markdown (.md) files within a given folder
# folder_path: String specifying the path to the folder containing the Markdown files
def count_words_in_folder(folder_path)
  unless Dir.exist?(folder_path)
    puts "Error: The folder '#{folder_path}' does not exist."
    exit(1)
  end

  md_files = Dir.glob("#{folder_path}/*.md")
  if md_files.empty?
    puts "Error: No Markdown files found in '#{folder_path}'."
    exit(1)
  end

  md_files.reduce(0) { |sum, file| sum + File.read(file).split.size }
end

# Counts the number of Markdown (.md) files in the specified folder
# folder_path: String with the path to the folder to be checked
def count_files_in_folder(folder_path)
  # Uses a glob pattern to find all Markdown files and returns the count
  Dir.glob("#{folder_path}/*.md").size
end

# Formats a number with underscores as thousands separators
# num: Integer to format
def format_with_underscores(num)
  # Converts the number to a String, reverses it, inserts an underscore every three digits, then reverses it back to normal order
  num.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1_').reverse
end

# Processes a folder of Markdown files, counts files and words, formats the numbers, and sends a notification
# folder_path: String specifying the path to the folder to process
# title: String specifying the title for the notification
# id_prefix: String that is prefixed to the notification id to make it unique
def process_folder_and_notify(folder_path, title, id_prefix)
  # Counts the number of files and words in the folder
  num_files = count_files_in_folder(folder_path)
  num_words = count_words_in_folder(folder_path)

  # Formats file and word counts with underscores
  formatted_files = format_with_underscores(num_files)
  formatted_words = format_with_underscores(num_words)

  # Sends a notification with the formatted file and word counts, including a title and a unique notification id
  show_notification("Pages: #{formatted_files}\nWords: #{formatted_words}",
                    title = title,
                    id = "#{id_prefix}-23-45834958")
end

# Constants for the paths to specific folders that contain Markdown files
BLOG_FILES_PATH = "#{Dir.home}/storage/shared/Documents/obsidian-brain/Brian".freeze
WRITING_PATH = "#{BLOG_FILES_PATH}/Blog/Writing".freeze
PUBLISHED_PATH = "#{BLOG_FILES_PATH}/Blog/Published".freeze
MORNING_PAGES = "#{BLOG_FILES_PATH}/Journal".freeze

# Processes the specified folders and sends notifications with counts for each
# process_folder_and_notify(MORNING_PAGES, "Morning Pages", "morning-pages")
# process_folder_and_notify(WRITING_PATH, "[Writing] Blog Posts", "blog-posts")
# process_folder_and_notify(PUBLISHED_PATH, "[Published] Blog Posts", "published-blog-posts")

# Sum up the total number of words across all three folders and format this number
# total_num_words = format_with_underscores(count_words_in_folder(MORNING_PAGES).to_i +
#                                           count_words_in_folder(WRITING_PATH).to_i +
#                                           count_words_in_folder(PUBLISHED_PATH).to_i)
total_num_words = format_with_underscores(count_words_in_folder(MORNING_PAGES).to_i)

# Sets a goal for the total number of words
goal = 1_000_000
# Calculate the percentage of the total word count towards the goal and format it to two decimal places
formatted_percentage = format("%.2f", (total_num_words.to_f / goal) * 100)

# Sends a final notification with the total word count and the percentage towards the goal
show_notification("Total Words: #{total_num_words}\nPercentage to 1mil: #{formatted_percentage}%",
                  title = "Total Words",
                  id = "total-words-23-45834958")
